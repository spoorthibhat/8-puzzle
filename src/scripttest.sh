#!/bin/bash
javac -cp "C:\Users\spoor\Desktop\EclipseWorkspace\8Puzzle\lib\algs4.jar;." Solver.java
for number in {01..40}
do
echo "Executing input${number}"
java -cp "C:\Users\spoor\Desktop\EclipseWorkspace\8Puzzle\lib\algs4.jar;." -Xmx1600m Solver "C:\\Users\\spoor\\Downloads\\8puzzle-testing\\8puzzle\\puzzle${number}.txt"
done
exit 0
