import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.MinPQ;
import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdOut;

/**---------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Written: 10/29/2017
 * Purpose: To solve the 8puzzle using A* algorithm.
 ---------------------------------------------------------------------------*/
public class Solver {

    private boolean solveable; // to check if the board is solveable.
    private  SearchNode goalNode; // the goal board.

    /**
     * A search node of the game to be a board, the number of moves made to
     * reach the board, and the predecessor search node.
     */
    private class SearchNode {

        private Board puzzleBoard; // board.
        private int numOfMoves; // moves of the search node.
        private SearchNode predecessor; // pred node.
        private int manhattan; // manhattan value.

        /**
         * Constructor to initialise the values of the Searchnode.
         * @param board input board.
         */
        SearchNode(final Board board) {
            puzzleBoard = board;
            numOfMoves = 0;
            predecessor = null;
            manhattan = puzzleBoard.manhattan();
        }
    }

    /**
     * To find the solution to the initial board using
     * A* algorithm.
     * @param initial - the initial board.
     */
    public Solver(Board initial) {

        if (initial == null) {
            throw new IllegalArgumentException("Passed board is null!");
        } else {
            solveable = false;
            MinPQ<SearchNode> pq = new MinPQ<SearchNode>(manhattanPriority());
            MinPQ<SearchNode> twinPQ = new MinPQ<SearchNode>(
                    manhattanPriority());
            // To keep track of dequed items.
            List<Board> dequedBoardList = new ArrayList<Board>();
            List<Board> twinDeqList = new ArrayList<Board>();
            Board twinBoard = initial.twin();
            SearchNode inputSearchNode = new SearchNode(initial);
            SearchNode inputSearchTwinNode = new SearchNode(twinBoard);

            pq.insert(inputSearchNode);
            twinPQ.insert(inputSearchTwinNode);

            SearchNode dequedSearchNode = pq.delMin();
            dequedBoardList.add(dequedSearchNode.puzzleBoard);

            SearchNode dequedTwin = twinPQ.delMin();
            twinDeqList.add(dequedTwin.puzzleBoard);

            while (!dequedSearchNode.puzzleBoard.isGoal() && !dequedTwin.puzzleBoard.isGoal()) {

                for (Board neighborBoard: dequedSearchNode.puzzleBoard.neighbors()) {
                    if ((dequedSearchNode.predecessor!= null && dequedSearchNode.predecessor.puzzleBoard.equals(neighborBoard))) {
                    	continue;
                    }
                    SearchNode neighborSearchNode = new SearchNode(neighborBoard);
                    neighborSearchNode.numOfMoves = dequedSearchNode.numOfMoves + 1;
                    neighborSearchNode.predecessor = dequedSearchNode;
                    pq.insert(neighborSearchNode);
                }

                for (Board twinNeighbor : dequedTwin.puzzleBoard.neighbors()) {
                    if ((dequedTwin.predecessor!= null && dequedTwin.predecessor.puzzleBoard.equals(twinNeighbor))) {
                        continue;
                    }
                    SearchNode twinNeighborNode = new SearchNode(twinNeighbor);
                    twinNeighborNode.numOfMoves = dequedTwin.numOfMoves + 1;
                    twinNeighborNode.predecessor = dequedTwin;
                    twinPQ.insert(twinNeighborNode);
                }
                dequedSearchNode = pq.delMin();
                dequedBoardList.add(dequedSearchNode.puzzleBoard);
                dequedTwin = twinPQ.delMin();
                twinDeqList.add(dequedTwin.puzzleBoard);
            }

            if (dequedSearchNode.puzzleBoard.isGoal()) {
                solveable = true;
                goalNode = dequedSearchNode;
            }
        }
    }

    /**
     * To find if the initail board is solveable.
     * @return true if solveable, else false.
     */
    public boolean isSolvable()  {
        // is the initial board solvable?
        return solveable;
    }

    /**
     * To compute min number of moves to solve initial board.
     * -1 if unsolvable
     * @return the nummber of moves.
     */
    public int moves() {
        // min number of moves to solve initial board; -1 if unsolvable
        if (isSolvable()) {
            return goalNode.numOfMoves;
        }
        return -1;
    }

    /**
     * Comparator for the priority queue.
     * @return the comparator.
     */
    private Comparator<SearchNode> manhattanPriority() {
        return new PriorityComparator();
    }

    /**
     * This is the comparator required for priority queue
     * operations based on this priority.
     * Priority = manhattan + number of moves.
     */
    private class PriorityComparator implements Comparator<SearchNode> {

        /**
         * Compare one node with another.
         * @param first node 1.
         * @param second node 2.
         * @return comparison result.
         */
        public int compare(final SearchNode first, final SearchNode second) {
            int firstPririty = first.manhattan + first.numOfMoves;
            int secPriority = second.manhattan + second.numOfMoves;

            if (firstPririty == secPriority) {
                return 0;
            } else if (firstPririty < secPriority) {
                return -1;
            } else {
                return +1;
            }
        }
    }

    /**
     * Sequence of boards in a shortest solution; null if unsolvable.
     * @return solutions.
     */
    public Iterable<Board> solution() {

        Stack<Board> solutions = new Stack<Board>();
        if (isSolvable()) {

            SearchNode tempGoal = goalNode;
            while (tempGoal.predecessor != null) {
                solutions.push(tempGoal.puzzleBoard);
                tempGoal = tempGoal.predecessor;
            }
            solutions.push(tempGoal.puzzleBoard);
            return solutions;
        }
        return null;

    }

    /**
     * Create the initial board from file and solve the puzzle.
     * @param args - the input file.
     */
    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int n = in.readInt();
        int[][] blocks = new int[n][n];
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n; j++)
                blocks[i][j] = in.readInt();
        long startTime = System.currentTimeMillis();

        Board initial = new Board(blocks);

        // solve the puzzle
        Solver solver = new Solver(initial);

        long endTime = System.currentTimeMillis();
        System.out.println("Time Taken:" + (endTime - startTime)/1000.0);
        
        // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
            //for (Board board : solver.solution())
            //    StdOut.println(board);
        }
    }
}