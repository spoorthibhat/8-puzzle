import edu.princeton.cs.algs4.Stack;
import edu.princeton.cs.algs4.StdRandom;

/**---------------------------------------------------------------------------
 * Author: Spoorthi Bhat
 * Written: 10/29/2017
 * Purpose: To create an immutable data type Board that takes in an n-by-n
 * array of blocks and compute the hamming and manhattan distances which are 
 * later used to solve an 8puzzle. This program also returns the neighbors of
 * one board.
 ---------------------------------------------------------------------------*/
public class Board {
	
	private int[][] blocksArray; // n-by-n array of blocks.
	private int boardSize; // the size of the board.
	
	/**
	 * Constructor to construct a board from an n-by-n array of blocks.
	 * where blocks[i][j] = block in row i, column j.
	 * @param blocks - input blocks array.
	 */
	public Board(int[][] blocks) {
		 
		if (blocks == null) {
			throw new IllegalArgumentException("Initial board is null.");
		} else {
			
			
			boardSize = blocks.length;
			blocksArray = new int[boardSize][boardSize];
			for (int i = 0; i < boardSize; i++) {
				for (int j = 0; j < boardSize; j++) {
					blocksArray[i][j] = blocks[i][j];
				}
			}
		}
	}
	
	/**
	 * To compute the dimension of the board.
	 * @return the dimension.
	 */
	public int dimension() {
		// board dimension n
		return boardSize;
	}
	
	/**
	 * To compute the original integer in the required block.
	 * @param row
	 * @param col
	 * @return
	 */
	private int correctPosition(int row, int col) {
		if ( row == boardSize-1 && col == boardSize - 1) {
			return 0;
		}
		return boardSize*row + col + 1;
	}
	public int hamming() {
		// number of blocks out of place
		int hammingCount = 0;
		for (int i = 0; i < boardSize; i++) {
			for (int j = 0; j < boardSize; j++) {
				if (blocksArray[i][j] != 0 && blocksArray[i][j] != (correctPosition(i, j))) {
					hammingCount++;
				}
			}
		}
		
		return hammingCount;
	}
	public int manhattan() {
		// sum of Manhattan distances between blocks and goal
		int manhattanDistance = 0;
		for (int r = 0; r < boardSize; r++) {
			for (int c = 0; c < boardSize; c++) {
				
				if (blocksArray[r][c] != 0) {
				int targetR = (blocksArray[r][c] - 1) / boardSize;
				int targetC = (blocksArray[r][c] - 1) % boardSize;
				manhattanDistance += Math.abs(r - targetR) + Math.abs(c - targetC);
				}
			}
		}
		
		return manhattanDistance;
	}
	public boolean isGoal()  {
		// is this board the goal board?
		for ( int row = 0; row < boardSize; row ++) {
			for (int col = 0; col < boardSize; col++) {
				if (blocksArray[row][col] != correctPosition(row, col)) {
					return false;
				}
			}
		}
		return true;
	}
	public Board twin() {
		// a board that is obtained by exchanging any pair of blocks
		Board twinBoard = new Board(blocksArray);
		int index = 0;
//		while(true) {
//		if (twinBoard.blocksArray[index][index + 1] != 0 && twinBoard.blocksArray[index + 1][index] != 0) {
//			exchange(twinBoard.blocksArray, index, index + 1, index + 1, index);
//			break;
//		}
//		index++;
			if(twinBoard.blocksArray[0][0] != 0 && twinBoard.blocksArray[0][1] != 0) {
				exchange(twinBoard.blocksArray, 0, 0, 0, 1);
			} else {
				exchange(twinBoard.blocksArray, 1, 1, 1, 0);
			}
//			break;
//		}
		return twinBoard;
	}
	
	private void exchange(int[][] inputArray, int r1, int c1, int r2, int c2) {
		int temp = inputArray[r1][c1];
		inputArray[r1][c1] = inputArray[r2][c2];
		inputArray[r2][c2] = temp;
	}
	public boolean equals(Object y) {
		// does this board equal y?
		if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        
        Board that = (Board) y;
        
        for (int i = 0; i < this.blocksArray.length; i++) {
			for (int j = 0; j < this.blocksArray[0].length; j++) {
				if (this.blocksArray[i][j] != that.blocksArray[i][j]) {
					return false;
				}
			}
        }
        return true;
	}
	public Iterable<Board> neighbors()  {
		// all neighboring boards
		Stack<Board> neighbor = new Stack<Board>();
		int row = 0;
		int col = 0 ;
		//int[][] helperBlocks = new int[boardSize][boardSize];
		for (int r = 0; r < boardSize; r++) {
			for (int c = 0; c < boardSize; c++) {
				//helperBlocks[r][c] = blocksArray[r][c];
				if(blocksArray[r][c] == 0) {
					row = r;
					col = c;
					break;
				}
			}
		}
		
		if (row > 0) { // swap with top
			exchange(blocksArray, row, col, row - 1, col);
			neighbor.push(new Board(blocksArray));
			exchange(blocksArray, row, col, row - 1, col);
		}
		
		if ( row < boardSize - 1) { // swap with bottom
			exchange(blocksArray, row, col, row + 1, col);
			neighbor.push(new Board(blocksArray));
			exchange(blocksArray, row, col, row + 1, col); // to get back to original
		}
		
		if ( col > 0) { // swap with left block
			exchange(blocksArray, row, col, row, col - 1);
			neighbor.push(new Board(blocksArray));
			exchange(blocksArray, row, col, row, col - 1);
		}
		
		if ( col < boardSize - 1) { // swap with right block
			exchange(blocksArray, row, col, row, col + 1);
			neighbor.push(new Board(blocksArray));
			exchange(blocksArray, row, col, row, col + 1);
		}
		
		return neighbor;
	}
	public String toString() {
		// string representation of this board (in the output format specified below)
		 StringBuilder s = new StringBuilder();
		    s.append(boardSize + "\n");
		    for (int i = 0; i < boardSize; i++) {
		        for (int j = 0; j < boardSize; j++) {
		            s.append(String.format("%2d ", blocksArray[i][j]));
		        }
		        s.append("\n");
		    }
		    return s.toString();
	}

	public static void main(String[] args) {
		// unit tests (not graded)
		
		int[][] init = {{8,1,3},{4,0,2},{7,6,5}};
		int[][] init2 = {{8,1,3},{4,0,2},{7,6,5}};

//		int[][] init2 = {{1,2,3},{4,5,6},{7,8,0}};
		Board myboard = new Board(init);
		Board myBoard2 = new Board(init2);
		System.out.println("Answer" + myBoard2.equals(myboard));
		
//		if(myboard.isGoal()) {
//			System.out.println("true");
//		}else{
//		System.out.println("false");
//		}
//		for (Board i : myboard.neighbors()) {
//		System.out.println(i);
//		}
	}

}
